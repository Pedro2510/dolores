<?php
session_start();
if(!$_SESSION['logadoAdmin'] == 'logadoAdmin'){
    header('Location: ../index.php');
  }else{
    
    include('../../backEnd/conexao.php');
    $sql = $conn ->prepare("SELECT NOME_FUNCIONARIO FROM farmadolores.tb_funcionarios WHERE MATRICULA_FUNCIONARIO = 1");
    $sql -> execute();
  
    if($dados = $sql -> fetch(PDO::FETCH_ASSOC)){
      $nome =  $dados["NOME_FUNCIONARIO"];
    }
  }



?>


<!DOCTYPE html>
<html lang="pt-Br">

<head>
    <meta charset="UTF-8" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin Dolores</title>
    <link rel="stylesheet" href="../CSS/statusPagamento.css" />
    <link
        href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
        rel="stylesheet" />
</head>

<body>
    <header>
        <div id="headerAdmin">
            <img src="../../img/logo.jpeg" alt="" id="headerLogo" />
            <p id="labelHeader">Acesso Administrativo</p>
        </div>
    </header>
    <main>
        <section id="sideBar">
            <li class="sidebarItems">
                <a href="./CadastrarCliente.php">
                    <img class="logoItems" src="../assets/img/CadastrarCliente.svg" alt="" />
                    <p>Cadastrar Cliente</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./ExcluirCliente.php">
                    <img class="logoItems" src="../assets/img/excluirCadastro.svg" alt="" />
                    <p>Excluir Cliente</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./ConsultarEstoque.php">
                    <img class="logoItems" src="../assets/img/pesquisa.svg" alt="" />
                    <p>Consultar Estoque</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./CadastrarProduto.php">
                    <img class="logoItems" src="../assets/img/cadastrarProduto.svg" alt="" />

                    <p>Cadastrar Produto</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./consultarPedido.php">
                    <img class="logoItems" src="../assets/img/pesquisa.svg" alt="" />
                    <p>Consultar Pedido</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./CadastrarFuncionario.html">
                    <img class="logoItems" src="../assets/img/funcionario.svg" alt="" />

                    <p>Cadastrar Funcionário</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./ExcluirFuncionario.html">
                    <img class="logoItems" src="../assets/img/excluirFuncionario.svg" alt="" />
                    <p>Excluir Funcionário</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./GerarNotaFiscal.php">
                    <img class="logoItems" src="../assets/img/notaFiscal.svg" alt="" />
                    <p>Gerar Nota Fiscal</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="./CriacaodeCupom.html">
                    <img class="logoItems" src="../assets/img/Cupom.svg" alt="" />
                    <p>Criação de Cupom</p>
                </a>
            </li>
            <li class="sidebarItems" id="opcaoEscolhida">
                <a href="./StatusdePagamentos.php">
                    <img class="logoItems" src="../assets/img/porquinho.svg" alt="" />
                    <p>Status de Pagamentos</p>
                </a>
            </li>
            <li class="sidebarItems">
                <a href="../../backEnd/logoutAdmin.php">
                    <img class="logoItems" src="../assets/img/Logout.svg" alt="" />
                    <p>Sair</p>
                </a>
            </li>
        </section>
        <section id="contentPage">
            <div id="contentBox">
                <p id="nameContent">Status de Pagamento</p>
                <table id="paymentStatusTable">
                    <tr>
                        <th class="headTable">Pedido</th>
                        <th class="headTable">Status</th>
                        <th class="headTable">Data</th>
                        <th class="headTable">Valor</th>
                        <th class="headTable">Ações</th>
                    </tr>
                    <?php 
                    
                    try {
                        $query = $conn->prepare("SELECT * FROM farmadolores.tb_pedidos where ID_STATUS = 6");
                        $query->execute();
                        //3.verificar se usuario e senah esta no banco de dados 
                        
                      } catch(PDOException $e) {
                        echo "Conexão falhou: " . $e->getMessage();
                      }

                      while($dados = $query -> fetch(PDO::FETCH_ASSOC)): 
                        $orgDate = $dados['DATA_PEDIDO'];  
                        $date = str_replace('-"', '/', $orgDate);  
                        $newDate = date("d/m/Y", strtotime($date));  
                    
                    ?>
                    <tr>
                        <div id="test">
                            <td class="contentTable"><?php echo $dados['ID_PEDIDO'] ?></td>
                            <td class="contentTable">
                                <div id="paymentMethod">
                                <?php if($dados['ID_STATUS'] == 6):?>
                                    <ul style="color: #F5A521">
                                      <li>Pagamento Efetuado</li>
                                  <?php endif;?>
                                    </ul>
                                </div>
                            </td>
                            <td class="contentTable"><?php echo $newDate; ?></td>
                            <td class="contentTable">R$ <?php echo $dados['VALOR_PEDIDO']; ?></td>
                            <td class="contentTable">
                                <div>
                                    <a href="aprovarCompra.php?idPedido=<?php echo $dados['ID_PEDIDO'] ?>" class="actionsTable">Aprovar</a>
                                    <a href="recusarCompra.php?idPedido=<?php echo $dados['ID_PEDIDO'] ?>" class="actionsTable">Recusar</a>
                                </div>
                            </td>
                        </div>
                    </tr>
                    <?php endwhile;?>
                </table>
            </div>
        </section>
    </main>
</body>

</html>