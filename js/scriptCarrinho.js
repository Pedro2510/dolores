document.getElementById('qtd').value = 1 
document.getElementById('subtotal').innerHTML += '62.50'
document.getElementById('subTotalValue').innerHTML += '62.50'
document.getElementById('totalPedidoValue').innerHTML += '62.50'

    function addRemove(){
      var count = 1;
      var limitMax = 9;
      var limitMin = 1;
      var btn = document.getElementById("pos");
      var disp = document.getElementById("qtd");
      var btnSub = document.getElementById("ant")
      
        btnSub.onclick = function () {
            if(count > limitMin){
              count--;
              document.getElementById('qtd').value = count
              removerValor(count)
            }
            if(count === 1){
              document.getElementById('ant').style.userSelect = 'none'
            }
        }
        btn.onclick = function () {
            if(count <= limitMax){
              count++;
              document.getElementById('qtd').value = count
              addValor(count)
            }
        }
    }
    function addValor(val){
      const valorPrincipal = document.getElementById('valorPrincipal').innerHTML
      
      document.getElementById('subtotal').innerHTML = new
         Intl.NumberFormat('pt-BR',{ minimumFractionDigits:2}).format(Number(valorPrincipal)*val)
      document.getElementById('subTotalValue').innerHTML = new
      Intl.NumberFormat('pt-BR',{ minimumFractionDigits:2}).format(Number(valorPrincipal)*val)
      document.getElementById('totalPedidoValue').innerHTML = new
      Intl.NumberFormat('pt-BR',{ minimumFractionDigits:2}).format(Number(valorPrincipal)*val)
    }
    function removerValor(val){
      const valorPrincipal = document.getElementById('valorPrincipal').innerHTML

      document.getElementById('subtotal').innerHTML = new
         Intl.NumberFormat('pt-BR',{ minimumFractionDigits:2}).format(Number(valorPrincipal)*val)
      document.getElementById('subTotalValue').innerHTML = new
         Intl.NumberFormat('pt-BR',{ minimumFractionDigits:2}).format(Number(valorPrincipal)*val)
      document.getElementById('totalPedidoValue').innerHTML = new
         Intl.NumberFormat('pt-BR',{ minimumFractionDigits:2}).format(Number(valorPrincipal)*val)
    }