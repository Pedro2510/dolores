<?php
session_cache_expire(180000);
session_start();
//se nao existir uma sessao aberta leva para o login


include('backEnd/conexao.php');





?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/produtosPadrao.css">
  <link
  href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
  rel="stylesheet"
  />  
  <title>Farma Dolores</title>
</head>
<body>
    <header>
        <div class="conteinner1">
          <a href="index.php">
            <img src="img/logo.jpeg" alt="Logo Farma Dolores" class="tamanhoLogo">
          </a>
          <div id="subContainner2">
            <form action="GET">
              <input type="text" name="pesquisa" id="pesquisa" placeholder="O que deseja encontrar ?">
              <span><img src="img/pesquisa.svg" alt="" class="tamanhoIcons" id="search"></span>
            </form>
          </div>
          <div id="subContainner3">
            <nav>
              <a href="backEnd/verificarLogin.php">
              <img src="img/user.svg" alt="user" class="tamanhoIcons">Minha Conta</a>
              <a href="meuPedidos.php">
                <img src="img/caixa.svg" alt="user" class="tamanhoIcons">Meus pedidos</a>
              <a href="carrinho.php">
                <img src="img/carrinho.svg" alt="user" class="tamanhoIcons">Meu Carrinho</a>
              <a href="pontos.html">
                <img src="img/carteira.svg" alt="user" class="tamanhoIcons">Meus Pontos</a>
            </nav>
          </div>
        </div>
        
        <div id="containner2">
          <ul>
            <li><a href="medicamentos.php">Medicamentos</a></li>
            <li><a href="saude.php">Saúde</a></li>
            <li><a href="beleza.php">Beleza</a></li>
            <li><a href="cuidadosDiarios.php">Cuidados Diários</a></li>
            <li><a href="infantil.html">Infantil</a></li>
            <li><a href="aragoDesmocosmeticos.html">Árago Dermocosméticos</a></li>
            <li><a href="preEposTreino.html">Pré e Pós Treino</a></li>
            <li><a href="diagnostico.html">Diagnóstico</a></li>
          </ul>
        </div>  

      </header>
      <main>      
        <div id="categoria">
          <h1>Medicamentos</h1>
          <hr>
        </div>
          <div class="produtosVendidos">  
          <?php

            try {
              $query = $conn->prepare("SELECT ID_PRODUTO,NOME_PRODUTO,PRECO_PRODUTO,CATEGORIA_PRODUTO,DS_PRODUTO,FOTO_PRODUTO from farmadolores.tb_estoque  WHERE CATEGORIA_PRODUTO = 'Medicamento';");
              $query->execute();
              //3.verificar se usuario e senah esta no banco de dados 
              
            } catch(PDOException $e) {
              echo "Conexão falhou: " . $e->getMessage();
            }

            while($dados = $query -> fetch(PDO::FETCH_ASSOC)):     
            ?>
            <div>
              <a href="detalheProduto.html">
                <img src="data:image/png;base64,<?php echo $dados['FOTO_PRODUTO'] ?>">
                              
                <h3><?php echo substr($dados['NOME_PRODUTO'], 0, 47),"..."?></h3>
              </a>
              <h2>R$ <?php echo $dados['PRECO_PRODUTO']?></h2>
              <p id="description"><?php echo  substr($dados['DS_PRODUTO'], 0, 90),"..."?></p>
              <a href="adicionarAoCarrinho.php?id=<?php echo  $dados['ID_PRODUTO'] ?>" type="button" type="submit" class="botaoCarrinho">
              ADICIONAR AO CARRINHO
              </a>
            </div>
            <?php
              endwhile;
            ?>
        </div>
           
           
          </div>

      </main>
      <footer>
        <div>
          <h2 class="colorBlue">FORMAS DE PAGAMENTO</h2>
          <h3 class="colorRed">
            Parcele em até 12x sem juros nos cartões de crédito.
          </h3>
          <img src="img/formas de pagamentos.PNG" class="imgFooter" />
          <h2 class="colorBlue">FORNECEDOR</h2>
          <img src="img/fornecedor.PNG" />
        </div>
        <div>
          <p class="colorBlue" id="thinBlue">Sempre ao seu lado</p>
          <a href="#" class="colorRed"><h2>www.farmadolores.com.br</h2></a>
          <h3 class="colorBlue" id="mediumBlue">
            Central de atendimento:
            <h2 class="colorRed"id="mediumBlue" >0800-4020</h2>
          </h3>
          <h2 class="colorRed" id="mediumBlue">atendimento@dolores.com.br</h2>
          <img src="img/logoFooter.PNG" />
        </div>
      </footer>
</body>
</html>