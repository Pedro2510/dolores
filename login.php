<?php
session_start();
  
  if(isset($_POST['user']) && isset($_POST['password'])) {
    //1.pega os valores do formulario
    $usuario = $_POST['user'];
    $senha = $_POST['password'];
    

    try {
        include('backEnd/conexao.php');
        $query = $conn->prepare("SELECT CPF_CLIENTE,SENHA_CLIENTE FROM farmadolores.tb_clientes WHERE CPF_CLIENTE=:CPF_CLIENTE and SENHA_CLIENTE= :SENHA_CLIENTE");
        $query ->bindParam(':CPF_CLIENTE',$usuario, PDO::PARAM_STR);
        $query ->bindParam(':SENHA_CLIENTE',$senha, PDO::PARAM_STR);
        $query->execute();
        

        //3.verificar se usuario e senah esta no banco de dados 
        $result = $query-> fetchAll();
        $qtd_usuarios = count($result);

        if($qtd_usuarios == 1){
            //TODO substituido pelo redirecionamento   
            $_SESSION['logado'] = 'logado';
            $_SESSION['cpf'] = $usuario;
            header('Location: index.php');
  
        }else if($qtd_usuarios == 0){
          $resultado["msg"] = "<div align='center' ><h3>Usu&aacute;rio e/ou senha inv&aacute;lido(s)!</h3></div>";
          $resultado["cod"] = 0;
        }  
       
    } catch(PDOException $e) {
        echo "Conexão falhou: " . $e->getMessage();
        }
  }
  if(isset($_POST['nome']) && isset($_POST['cpf']) && isset($_POST['data']) && isset($_POST['email']) && isset($_POST['senha']) && isset($_POST['telefone']) && isset($_POST['password2'])) {
    $nome = $_POST['nome'];
    $cpf = $_POST['cpf'];
    $data = $_POST['data'];
    $email = $_POST['email'];
    $senha = $_POST['senha'];
    $telefone = $_POST['telefone'];
    $confirmPassword = $_POST['password2'];

    try {
      include('backEnd/conexao.php');
      $query = $conn->prepare("INSERT INTO farmadolores.tb_clientes(CPF_CLIENTE, NOME_CLIENTE, EMAIL_CLIENTE, DATA_NASCIMENTO, TELEFONE_CLIENTE, SENHA_CLIENTE) 
        VALUES (:cpf,:nome,:email,:dataNascimento,:telefone,:senha)");
      $query ->bindParam(':cpf',$cpf, PDO::PARAM_STR);
      $query ->bindParam(':nome',$nome, PDO::PARAM_STR);
      $query ->bindParam(':email',$email, PDO::PARAM_STR);
      $query ->bindParam(':dataNascimento',$data, PDO::PARAM_STR);
      $query ->bindParam(':telefone',$telefone, PDO::PARAM_STR);
      $query ->bindParam(':senha',$senha, PDO::PARAM_STR);
      $query->execute();
      echo 'vc esta cadastrado';
      //3.verificar se usuario e senah esta no banco de dados 
      
  } catch(PDOException $e) {
      echo "Conexão falhou: " . $e->getMessage();
      }
    
  
  }


?>





<!DOCTYPE html>
<html lang="pt-Br">
  <head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="css/styleLogin.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
      rel="stylesheet"
    />
    <title>Login</title>
  </head>
  <body>
    <header>
      <header>
        <div class="conteinner1">
          <a href="index.php">
            <img src="img/logo.jpeg" alt="Logo Farma Dolores" class="tamanhoLogo">
          </a>
          <div id="subContainner2">
            <form action="GET">
              <input type="text" name="pesquisa" id="pesquisa" placeholder="O que deseja encontrar ?">
              <span><img src="img/pesquisa.svg" alt="" class="tamanhoIcons" id="search"></span>
            </form>
          </div>
          <div id="subContainner3">
            <nav>
            <a href="minhaconta.html">
                <img src="img/user.svg" alt="user" class="tamanhoIcons">Minha Conta</a>
              <a href="meuPedidos.php">
                <img src="img/caixa.svg" alt="user" class="tamanhoIcons">Meus pedidos</a>
              <a href="carrinho.php">
                <img src="img/carrinho.svg" alt="user" class="tamanhoIcons">Meu Carrinho</a>
              <a href="pontos.html">
                <img src="img/carteira.svg" alt="user" class="tamanhoIcons">Meus Pontos</a>
            </nav>
          </div>
        </div>    
    
    
      </header>
    </header>
    <main>
      <div id="containnerBox">
        <div id="formLogin" class="form">
          <h3 class="mainTextForm">Já sou cliente</h3>
          <p class="subTextForm">Faça seu login para acessar</p>
          <?php if(isset($resultado) && ($resultado["cod"] == 0)): ?>
            <div class="alert alert-danger">
                <?php echo $resultado["msg"]; ?>
            </div>
          <?php endif;?>
          <form action="login.php" method="POST">
            <label for="user">Email ou CPF*</label>
            <input type="text" name="user" id="user" class="inputData" />

            <label for="password">Senha*</label>
            <input
              type="password"
              name="password"
              id="password"
              class="inputData"
            />

            <div class="subLink">
              <a href="#">Esqueceu a senha?</a>
              <a href="#">Reenviar email?</a>
            </div>

            <button type="submit" id="loginButton" type="submit">Faça seu login</button type="submit">
          </form>
          
        </div>

        <div id="formCadastro" class="form">
          <h3 class="mainTextForm">Ainda não sou cliente</h3>
          <p class="subTextForm">
            Cadastre-se para receber novidades e finalizar sua compra
          </p>
          <form action="login.php" method="POST" id="dataForm">
            <input
              type="text"
              name="nome"
              id="nome"
              placeholder="Nome Completo:"
            />

            <div class="datasGroups">
              <input
                type="text"
                name="cpf"
                id="cpf"
                placeholder="CPF:"
                class="leftData"
              />
              <input
                type="date"
                name="data"
                id="data"
                placeholder="Data de nascimento:"
                class="rightData"
              />
            </div>
            <div class="datasGroups">
              <input
                type="text"
                name="email"
                id="email"
                placeholder="Email:"
                class="leftData"
              />
              <input
                type="password"
                name="senha"
                id="password"
                placeholder="Senha:"
                class="rightData"
              />
            </div>
            <div class="datasGroups">
              <input
                type="tel"
                name="telefone"
                id="telefone"
                placeholder="Telefone:"
                class="leftData"
              />
              <input
                type="password"
                name="password2"
                id="password2"
                placeholder="Confirme sua senha:"
                class="rightData"
              />
            </div>
            <button type="submit" id="cadastreSeButton" type="submit">Cadastre-se</button type="submit">
          </form>
        </div>
      </div>
    </main>
    <footer>
      <div>
        <h2 class="colorBlue">FORMAS DE PAGAMENTO</h2>
        <h3 class="colorRed">
          Parcele em até 12x sem juros nos cartões de crédito.
        </h3>
        <img src="img/formas de pagamentos.PNG" class="imgFooter" />
        <h2 class="colorBlue">FORNECEDOR</h2>
        <img src="img/fornecedor.PNG" />
      </div>
      <div>
        <p class="colorBlue" id="thinBlue">Sempre ao seu lado</p>
        <a href="#" class="colorRed"><h2>www.farmadolores.com.br</h2></a>
        <h3 class="colorBlue" id="mediumBlue">
          Central de atendimento:
          <h2 class="colorRed" id="mediumBlue">0800-4020</h2>
        </h3>
        <h2 class="colorRed" id="mediumBlue">atendimento@dolores.com.br</h2>
        <img src="img/logoFooter.PNG" />
      </div>
    </footer>
  </body>
</html>
