<?php
session_cache_expire(180000);
session_start();
//se nao existir uma sessao aberta leva para o login


include('backEnd/conexao.php');

try {
  $cpf = $_SESSION['cpf'];
  $query2 = $conn->prepare("SELECT * from farmadolores.tb_carrinho LEFT JOIN farmadolores.tb_estoque ON tb_carrinho.ID_PRODUTO = tb_estoque.ID_PRODUTO where tb_carrinho.CPF_CLIENTE  = :CPF_CLIENTE;");
  $query2 ->bindParam(':CPF_CLIENTE',$cpf, PDO::PARAM_STR);
  $query2->execute();
  //3.verificar se usuario e senah esta no banco de dados 
  
} catch(PDOException $e) {
  echo "Conexão falhou: " . $e->getMessage();
}


  while($dados = $query2 -> fetch(PDO::FETCH_ASSOC)){
    $_SESSION['qtdMax'][$dados['ID_PRODUTO']] = $dados['QTD_PRODUTO'];
    $_SESSION['preco'][$dados['ID_PRODUTO']] = $dados['PRECO_PRODUTO'];
    

    if(!isset($_SESSION['qtd'][$dados['ID_PRODUTO']])){
      $_SESSION['qtd'] = array();
      $_SESSION['qtd'][$dados['ID_PRODUTO']] = 1;
      
      
    }
  }   

  

if(isset($_GET['subId'])){
  $idProduto = $_GET['subId'];
  if($_SESSION['qtd'][$idProduto] != 1){
    $_SESSION['qtd'][$idProduto] -= 1;
    $_SESSION['preco'][$idProduto] = $_SESSION['preco'][$idProduto] * $_SESSION['qtd'][$idProduto];
    $_SESSION['precoTotal'] = $_SESSION['preco'][$idProduto];
  }
}

if(isset($_GET['addId'])){
  $idProduto = $_GET['addId'];
  if($_SESSION['qtd'][$idProduto] != $_SESSION['qtdMax'][$idProduto]){
    $_SESSION['qtd'][$idProduto] += 1;
    $_SESSION['preco'][$idProduto] = $_SESSION['preco'][$idProduto] * $_SESSION['qtd'][$idProduto];
    $_SESSION['precoTotal'] = $_SESSION['preco'][$idProduto];
  }
}





?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/styleCarrinho.css">
  
  <title>Document</title>
</head>
<body>
  <header>
    <div class="conteinner1">
      <a href="index.php">
        <img src="img/logo.jpeg" alt="Logo Farma Dolores" class="tamanhoLogo">
      </a>
      <div id="subContainner2">
        <form action="GET">
          <input type="text" name="pesquisa" id="pesquisa" placeholder="O que deseja encontrar ?">
          <span><img src="img/pesquisa.svg" alt="" class="tamanhoIcons" id="search"></span>
        </form>
      </div>
      <div id="subContainner3">
        <nav>
        <a href="backEnd\verificarLogin.php">
            <img src="img/user.svg" alt="user" class="tamanhoIcons">Minha Conta</a>
          <a href="meuPedidos.php">
            <img src="img/caixa.svg" alt="user" class="tamanhoIcons">Meus pedidos</a>
          <a href="carrinho.php">
            <img src="img/carrinho.svg" alt="user" class="tamanhoIcons">Meu Carrinho</a>
          <a href="pontos.html">
            <img src="img/carteira.svg" alt="user" class="tamanhoIcons">Meus Pontos</a>

        </nav>
      </div>
    </div>
    <hr>    
  </header>
  <main>
    <h2 class="titleCard">Carrinho de compras</h2>
    <div id="carrinhoCompras">
      
      <div id="compraFinal">
        <div id="headerTb">
          <ul>
            <li class="first">Produtos</li>
            <li class="two">Preço</li>
            <li class="three">quantidade</li>
            <li class="for">SubTotal</li>
          </ul>
        </div>
        
        <?php

          try {
            $cpf = $_SESSION['cpf'];
            $query = $conn->prepare("SELECT * from farmadolores.tb_carrinho LEFT JOIN farmadolores.tb_estoque ON tb_carrinho.ID_PRODUTO = tb_estoque.ID_PRODUTO where tb_carrinho.CPF_CLIENTE  = :CPF_CLIENTE;");
            $query ->bindParam(':CPF_CLIENTE',$cpf, PDO::PARAM_STR);
            $query->execute();
            //3.verificar se usuario e senah esta no banco de dados 
            
          } catch(PDOException $e) {
            echo "Conexão falhou: " . $e->getMessage();
            }


            while($dados = $query -> fetch(PDO::FETCH_ASSOC)):     
              $_SESSION['qtdMax'][$dados['ID_PRODUTO']] = $dados['QTD_PRODUTO'];
          ?>
          <div class="itemPedido">
            <ul>
              <li class="first">
                <img class="icon" src="data:image/png;base64,<?php echo $dados['FOTO_PRODUTO'] ?>">
                <p><?php echo substr($dados['NOME_PRODUTO'], 0, 25),"..."?></p>
              </li>
              <li class="two">
                R$
                <div  id="valorPrincipal">
                  <?php echo $dados['PRECO_PRODUTO']?>
                </div>
              </li>
              <li class="three">

                <a type="submit" id="ant" href="carrinho.php?subId=<?php echo $dados['ID_PRODUTO']?>">
                  <img class="botaoADDREMOVE" src="img/diminuir.svg">
                </a>

                <input name="qtd" id="qtd" min="0" max="<?php echo $dados['QTD_PRODUTO']?>" value="<?php echo $_SESSION['qtd'][$dados['ID_PRODUTO']] ?>">

                <a type="submit" id="pos" href="carrinho.php?addId=<?php echo $dados['ID_PRODUTO']?>">
                  <img class="botaoADDREMOVE" src="img/adicionar.svg">
                </a type="submit">

              </li>
             
              <li class="for">
                R$  <?php echo $_SESSION['preco'][$dados['ID_PRODUTO']]?>
                <div id="subtotal">

                </div>
              </li>
              <li id="trash">
                <a href="removerDoCarrinho.php?id=<?php echo  $dados['ID_PRODUTO']?>&valorProduto=<?php echo $_SESSION['preco'][$dados['ID_PRODUTO']]?>">
                  <img class="icon" src="img/lixeira.svg">
                </a>
              </li>
            </ul>
            </div>
            <?php
            endwhile;
          ?>      
        
        
      </div>
      <div id="finalizarCompra">
        <form action="backEnd/finalizarCompra.php" method="POST">
        <div id="containnerPrinc">
          <div id="frete">
            <a class="link dropDesc" href="#">
              Consultar Valor do Frete e imposto 
              <img class="dropDownFinal" src="img/arrow_Rigth_Final_Compra.svg" >
            </a>
            <div id="buscaCep">
              <input type="text" name="CEP" id="cep" required minlength="8"maxlength="8">
              <a class="link" href="#">Não sabe o CEP?</a>
            </div>
          </div>
          <hr>
          <div id="consultaCupom">
            <a  class="link dropDesc" href="#">Aplicar Cupom de Desconto
              <img class="dropDownFinal" src="img/arrow_Rigth_Final_Compra.svg">
            </a>
            <hr>
            <div>
              <input type="text" name="CUPOM" id="cupom" placeholder="INSERIR CUPOM DE DESCONTO" >
              <a class="link caixaBotao" href="#"></aclass>
                <p>
                    APLICAR DESCONTO
                </p>
              </a>
              
            </div>
          </div>
          <hr>
          <div id="resumoDaCompra">
            <div id="subTotal">
              <p>Sub-Total</p>
              <div>
                <p>
                  R$ <?php echo $_SESSION['precoTotal']?>
                  <p id="subTotalValue">
                  </p>
                </p>
              </div>
              
            </div>
            <div id="taxa">
              <p>Taxa</p>
              <p>R$ 0,00</p>
            </div>
          </div>
          <hr>
          <div id="resumoDoPedido">
            <div id="totalPedido">
              <h3>Total do Pedido</h3>
              <div>
                <p>
                  R$ <?php echo $_SESSION['precoTotal']?>
                  <p id="totalPedidoValue">
                  </p>
                </p>
              </div>
              
            </div>
          </div>
        </div>
        
        <button  class="link caixaBotao" id="comprar" type="submit">FINALIZAR COMPRA</button>

        </form>
      </div>
      
    </div>
  </main>

  <footer>
    <div>
      <h2 class="colorBlue">FORMAS DE PAGAMENTO</h2>
      <h3 class="colorRed">Parcele em até 12x sem juros nos cartões de crédito.</h3>
      <img src="img/formas de pagamentos.PNG" class="imgFooter">
      <h2 class="colorBlue">FORNECEDOR</h2>
      <img src="img/fornecedor.PNG">
    </div>
    <div>
      <p class="colorBlue">Sempre ao seu lado</p>
      <a href="#" class="colorRed"><h2>www.farmadolores.com.br</h2></a>
      <h3 class="colorBlue">Central de atendimento:<h2 class="colorRed">0800-4020</h2></h3>
      <h2 class="colorRed">atendimento@dolores.com.br</h2>
      <img src="img/logoFooter.PNG">
    </div>

  </footer>
  
</body>
</html>
