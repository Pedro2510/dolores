<?php

session_start();


if(isset($_GET['idPedido'])){
  $idPedido = $_GET['idPedido'];
}else{
  $idPedido = $_SESSION['idPedidoCompra'];
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css\pagamento.css">
  <title>pagamento</title>
</head>
<body>
  <div class="pagamento">
    <p>Ola, tudo bem? so falta mais um passo para finalizar sua compra nesta tela você pode fazer o pagamento ou voltar para seus pedidos.</p>
    <img src="img\flowcode.png" class="qrcode">
    <div class="opcaoPag">
      <a href="validarPagamento.php?pagFalse='false'">ver meus pedidos</a>
      <a href="validarPagamento.php?pagTrue=<?php echo $idPedido ?>">pagamento efetuado!</a>
    </div>
  </div>
</body>
</html>