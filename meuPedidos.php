<?php
session_start();

include('backEnd/conexao.php');

?>


<!DOCTYPE html>
<html lang="pt-Br">
  <head>
    <meta charset="UTF-8" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/stylePedidos.css">
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
      rel="stylesheet"
    />
    <title>Document</title>
  </head>
  <body>
    <header>
      <div class="conteinner1">
        <a href="index.php">
          <img src="img/logo.jpeg" alt="Logo Farma Dolores" class="tamanhoLogo">
        </a>
        <div id="subContainner2">
          <form action="GET">
            <input type="text" name="pesquisa" id="pesquisa" placeholder="O que deseja encontrar ?">
            <span><img src="img/pesquisa.svg" alt="" class="tamanhoIcons" id="search"></span>
          </form>
        </div>
        <div id="subContainner3">
          <nav>
          <a href="meuPedidos.php">
                <img src="img/user.svg" alt="user" class="tamanhoIcons">Minha Conta</a>
              <a href="meuPedidos.php">
                <img src="img/caixa.svg" alt="user" class="tamanhoIcons">Meus pedidos</a>
              <a href="carrinho.php">
                <img src="img/carrinho.svg" alt="user" class="tamanhoIcons">Meu Carrinho</a>
              <a href="pontos.html">
              <img src="img/carteira.svg" alt="user" class="tamanhoIcons">Meus Pontos</a>
  
          </nav>
        </div>
      </div>
      <hr>    
    </header>
    <main>
      <div id="menu">
        <ul>
          <li><a href="backEnd/verificarLogin.php">
            <img src="img/meus-pedidos.svg" class="icons-menu">Meus pedidos</a></li>
          <li><a href="#">
            <img src="img/meus-beneficios.svg" class="icons-menu">Meus benefícios</a></li>
          <li><a href="#">
            <img src="img/meus-dados.svg" class="icons-menu">Meus dados</a></li>
          <li><a href="#">
            <img src="img/meus-enderecos.svg" class="icons-menu">Meus endereços</a></li>
          <li><a href="#">
            <img src="img/favoritos.svg" class="icons-menu">Favoritos</a></li>
          <li><a href="#">
            <img src="img/alterar-senha.svg" class="icons-menu">Alterar senha</a></li>
          <li><a href="backEnd\logout.php">
            <img src="img/sair.svg" class="icons-menu">Sair</a></li>
        </ul>
      </div>
          <section id="contentPage">
            <div id="contentBox">
                <p id="nameContent">Status de Pagamento</p>
                <table id="paymentStatusTable">
                    <tr>
                        <th class="headTable">Pedido</th>
                        <th class="headTable">Status</th>
                        <th class="headTable">Data</th>
                        <th class="headTable">Valor</th>
                        <th class="headTable"></th>
                    </tr>
                    <?php
                      $cpfCliente = $_SESSION['cpf'];
                      try {
                        $query = $conn->prepare("SELECT * FROM farmadolores.tb_pedidos where CPF_CLIENTE = '$cpfCliente';");
                        $query->execute();
                        //3.verificar se usuario e senah esta no banco de dados 
                        
                      } catch(PDOException $e) {
                        echo "Conexão falhou: " . $e->getMessage();
                      }

                      while($dados = $query -> fetch(PDO::FETCH_ASSOC)): 
                        $orgDate = $dados['DATA_PEDIDO'];  
                        $date = str_replace('-"', '/', $orgDate);  
                        $newDate = date("d/m/Y", strtotime($date));  
                       
                        
                    ?>
                    <tr>
                        <div id="test">
                            <td class="contentTable"><?php echo $dados['ID_PEDIDO'] ?></td>
                            <td class="contentTable">
                                <div id="paymentMethod">
                                  <?php if($dados['ID_STATUS'] == 1):?>
                                    <ul style="color: black">
                                      <li>Aguardando Pagamento</li>
                                  <?php elseif($dados['ID_STATUS'] == 2):?>   
                                    <ul style="color: green">
                                      <li>Pagamento Confirmado</li>
                                  <?php elseif($dados['ID_STATUS'] == 3):?>   
                                    <ul style="color: red">
                                      <li>Cancelado</li>
                                  <?php elseif($dados['ID_STATUS'] == 4):?>   
                                    <ul style="color: #0014ff">
                                      <li>Entregue</li>
                                  <?php elseif($dados['ID_STATUS'] == 5):?>   
                                    <ul style="color: #0089ff">
                                      <li>Em transporte</li>
                                  <?php elseif($dados['ID_STATUS'] == 6):?>   
                                    <ul style="color: #ffbc00">
                                      <li>Em Analise</li>
                                  <?php endif;?>
                                  
                                    </ul>
                                </div>
                            </td>
                            <td class="contentTable"><?php echo $newDate; ?></td>
                            <td class="contentTable">R$ <?php echo $dados['VALOR_PEDIDO']; ?></td>
                            <?php if($dados['ID_STATUS'] == 6):?>
                              <td class="contentTable">
                                  <div>
                                      <a href="pagamento.php?idPedido=<?php echo $dados['ID_PEDIDO']?>" class="actionsTable">Detalhes</a>
                                  </div>
                              </td>
                            <?php endif;?>
                        </div>
                    </tr>
                    <?php
                      endwhile;
                    ?>
                </table>
            </div>
        </section>
    </main>

    <footer>
      <div>
        <h2 class="colorBlue">FORMAS DE PAGAMENTO</h2>
        <h3 class="colorRed">
          Parcele em até 12x sem juros nos cartões de crédito.
        </h3>
        <img src="img/formas de pagamentos.PNG" class="imgFooter" />
        <h2 class="colorBlue">FORNECEDOR</h2>
        <img src="img/fornecedor.PNG" />
      </div>
      <div>
        <p class="colorBlue" id="thinBlue">Sempre ao seu lado</p>
        <a href="#" class="colorRed"><h2>www.farmadolores.com.br</h2></a>
        <h3 class="colorBlue" id="mediumBlue">
          Central de atendimento:
          <h2 class="colorRed"id="mediumBlue" >0800-4020</h2>
        </h3>
        <h2 class="colorRed" id="mediumBlue">atendimento@dolores.com.br</h2>
        <img src="img/logoFooter.PNG" />
      </div>
    </footer>
  </body>
</html>
